﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace stoktakip
{
    public class VwSTOKEKLE
    {
        public int ID
        {
            get;
            set;
        }
        public string STOK_ADI
        {
            get;
            set;
        }
        public string STOK_MODELI
        {
            get;
            set;
        }
        public int STOK_ADEDİ
        {
            get;
            set;
        }
    }
}