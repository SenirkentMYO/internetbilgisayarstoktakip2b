﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="stoktakip._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 1056px;
            height: 425px;
        }
        .auto-style3 {
        }
        .auto-style4 {            height: 425px;
        }
        .auto-style5 {
            width: 1041px;
            height: 280px;
        }
        .auto-style6 {
            height: 23px;
        }
        .auto-style7 {
            width: 992px;
            height: 23px;
        }
        .auto-style8 {
            width: 1056px;
            height: 23px;
        }
        .auto-style10 {
            width: 395px;
        }
        .auto-style11 {
            width: 302px;
        }
        .auto-style12 {
            width: 302px;
            height: 23px;
        }
        .auto-style13 {
            width: 395px;
            height: 23px;
        }
        .auto-style14 {
            width: 81px;
        }
        .auto-style16 {
            width: 463px;
            height: 65px;
        }
        .auto-style17 {
            width: 328px;
        }
        .auto-style18 {
            width: 220px;
        }
        .auto-style19 {
            width: 169px;
        }
        .auto-style20 {
            width: 475px;
        }
        .auto-style21 {
            width: 169px;
            height: 19px;
        }
    </style>
</head>
<body style="height: 835px; width: 1071px">
    <form id="form1" runat="server">
   
        <table>
            <tr>
                <td class="auto-style3" colspan="3">
                    <img alt="" class="auto-style5" src="images/banner-pc-repair-952x280.jpg" /></td>
            </tr>
            <tr>
                <td class="auto-style6" colspan="3">
                    <table>
                        <tr>
                            <td class="auto-style18">
                                <asp:TextBox ID="txtgiriş" runat="server"></asp:TextBox>
                            </td>
                            <td class="auto-style17" rowspan="2">
                                <asp:Button ID="Button1" runat="server" Height="32px" Text="GİRİŞ" Width="110px" OnClick="Button1_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style18">
                                <asp:TextBox ID="txtşifre" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style18">
                                <asp:Label ID="lblmesaj" runat="server" Text="lblmesaj"></asp:Label>
                            </td>
                            <td class="auto-style17">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="auto-style4" colspan="2">
                    <table>
                        <tr>
                            <td class="auto-style12">
                                <asp:Label ID="lbl" runat="server" Text="STOKADİ"></asp:Label>
                            </td>
                            <td class="auto-style13">
                                <asp:TextBox ID="txt" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style11">
                                <asp:Label ID="lbl2" runat="server" Text="STOK MODELİ"></asp:Label>
                            </td>
                            <td class="auto-style10">
                                <asp:TextBox ID="txt1" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style11">
                                <asp:Label ID="Label1" runat="server" Text="STOK ADEDI"></asp:Label>
                            </td>
                            <td class="auto-style10">
                                <asp:TextBox ID="txt3" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <br />
                </td>
                <td class="auto-style1">
                    <table>
                        <tr>
                            <td class="auto-style14">
                                <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="Repeater1_ItemCommand">
                                    <HeaderTemplate>
                             <table>
                                 <tr>
                                     <td>STOK ADI</td>
                                     <td>STOK MODELI</td>
                                     <td>STOK ADEDI</td>
                                                 
                                 </tr>
                        </HeaderTemplate>
                    <ItemTemplate>
                       
                            <tr>
                                <td><%#Eval("STOK ADI") %></td>
                                <td><%#Eval("STOK MODELI") %></td>
                                <td><%#Eval("STOK ADEDI") %></td>
                                 </tr>
                       
                        </ItemTemplate>
                        <FooterTemplate>
                             </table>
                                
                            </FooterTemplate>
                                    </asp:Repeater>
                            </td>
                        </tr>
                   
                    </td>
            </tr>
            <tr>
                <td class="auto-style16">
                    <table>
                        <tr>
                            <td class="auto-style20">
                                <asp:Repeater ID="Repeater2" runat="server" OnItemCommand="Repeater1_ItemCommand">
                                    <HeaderTemplate>
                             <table>
                                 <tr>
                                     <td>Giriş Miktarı</td>
                                     <td>Toplam Miktar</td>
                                     <td>Çıkış Miktarı</td>
                                                 
                                 </tr>
                        </HeaderTemplate>
                    <ItemTemplate>
                       
                            <tr>
                                <td><%#Eval("Giriş Miktarı") %></td>
                                <td><%#Eval("Toplam Miktar") %></td>
                                <td><%#Eval("Çıkış Miktarı") %></td>
                                 </tr>
                       
                        </ItemTemplate>
                        <FooterTemplate>
                             </table>
                                
                            </FooterTemplate>
                                    </asp:Repeater>
                </td>
                <td class="auto-style7"></td>
                <td class="auto-style8">
                    <table>
                        <tr>
                            <td class="auto-style19">
                                <asp:Label ID="lbl5" runat="server" Text="STOK EKLE"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style19">
                                STOK MİKTARI</td>
                        </tr>
                        <tr>
                            <td class="auto-style21">
                                <asp:Label ID="lbl7" runat="server" Text="ÇIKIŞ"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
   
        <asp:Label ID="lblsayac" runat="server" Text="lbl"></asp:Label>
   
    </form>
</body>
</html>
